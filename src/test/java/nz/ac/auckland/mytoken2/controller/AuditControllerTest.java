package nz.ac.auckland.mytoken2.controller;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import nz.ac.auckland.mytoken2.domain.Audit;
import nz.ac.auckland.mytoken2.dto.AuditDetail;
import nz.ac.auckland.mytoken2.dto.AuditSearchCriteria;
import nz.ac.auckland.mytoken2.repository.AuditRepository;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class AuditControllerTest {
    @Autowired
    AuditController auditController;

    @Autowired
    AuditRepository auditRepository;

    @Autowired
    MockMvc mvc;

    private static List<Audit> mockData = new ArrayList<>();

    @BeforeClass
    public static void createData() {
        Audit audit1 = new Audit();
        Audit audit2 = new Audit();
        Audit audit3 = new Audit();

        audit1.setId((long)123456);
        audit1.setSummary("Added");
        audit1.setTokenType("YUBIKEY");
        audit1.setTokenId("123456");
        audit1.setAffectedUser("jblo123");
        audit1.setOperator("jblo123");
        audit1.setTimestamp(new Date());
        audit1.setOperatorIpAddress("10.1.1.1");
        mockData.add(audit1);

        audit2.setId((long)123457);
        audit2.setSummary("Deleted");
        audit2.setTokenType("GOOGLE");
        audit2.setAffectedUser("jblo123");
        audit2.setOperator("tell022");
        audit2.setTimestamp(new Date());
        audit2.setOperatorIpAddress("10.1.1.2");
        mockData.add(audit2);

        audit3.setId((long)123458);
        audit3.setSummary("Updated");
        audit3.setTokenType("YUBIKEY");
        audit3.setTokenId("123456");
        audit3.setAffectedUser("jblo123");
        audit3.setOperator("jblo123");
        audit3.setTimestamp(new Date());
        audit3.setOperatorIpAddress("10.1.1.1");
        mockData.add(audit3);
    }

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        auditRepository.saveAll(mockData);
    }

    @Test
    public void search_NoCriteria() {
        List<AuditDetail> result = auditController.search(new AuditSearchCriteria());
        assertThat("Returns full list", result.size(), is(3));
    }

    @Test
    public void search_ByOperator() {
        AuditSearchCriteria auditSearchCriteria = new AuditSearchCriteria();
        auditSearchCriteria.setOperator("tell022");

        List<AuditDetail> result = auditController.search(auditSearchCriteria);
        assertThat("Returns only one result", result.size(), is(1));
        assertThat("IP Address should be 10.1.1.2", result.get(0).getOperatorIpAddress(), is("10.1.1.2"));
    }

    @Test
    public void search_CaseInsensitiveUser() {
        AuditSearchCriteria auditSearchCriteria = new AuditSearchCriteria();
        auditSearchCriteria.setAffectedUser("JBLO123");

        List<AuditDetail> result = auditController.search(auditSearchCriteria);
        assertThat("Returns three results", result.size(), is(3));
    }
}