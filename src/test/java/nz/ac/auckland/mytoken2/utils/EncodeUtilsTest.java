package nz.ac.auckland.mytoken2.utils;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

import org.junit.Test;

public class EncodeUtilsTest {

    @Test
    public void testDecodeHexToByteArray() {
    }

    @Test
    public void testEncodeBytesToHex() {
    }

    @Test
    public void testModHexToHex() {
        assertThat("cbdefghijklnrtuv modhex should equal 0123456789abcdef in hex",
                EncodeUtils.modHexToHex("cbdefghijklnrtuv"), equalTo("0123456789abcdef"));
    }

    @Test
    public void testHexToModhex() {
        assertThat("0123456789abcdef hex should equal cbdefghijklnrtuv in modhex",
                EncodeUtils.hexToModhex("0123456789abcdef"), equalTo("cbdefghijklnrtuv"));
    }
}