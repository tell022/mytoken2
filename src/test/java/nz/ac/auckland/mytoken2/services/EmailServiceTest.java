package nz.ac.auckland.mytoken2.services;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.mail.MailSender;

public class EmailServiceTest {
    @Mock
    private MailSender mailSender;

    private EmailService emailService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        emailService = new EmailService(mailSender);
    }

    @Test
    public void testSendEmail() {
        // TODO: Test sent
        emailService.sendEmail("test@test.com");
    }
}