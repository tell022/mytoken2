package nz.ac.auckland.mytoken2.services;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import nz.ac.auckland.mytoken2.domain.Yubikey;
import nz.ac.auckland.mytoken2.repository.YubikeyRepository;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

public class YubikeyServiceTest {
    @Mock
    private NewYubikeyService newYubikeyService;

    @Mock
    private YubikeyRepository yubikeyRepository;

    private YubikeyService yubikeyService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        yubikeyService = new YubikeyService(yubikeyRepository, newYubikeyService);
    }

    @Test
    public void findBySerial() {
        Yubikey mockYubikey = new Yubikey();
        mockYubikey.setSerialNumber("123456");
        Mockito.when(yubikeyRepository.findBySerialNumber("123456")).thenReturn(mockYubikey);

        Yubikey yubikey = yubikeyService.findBySerial("123456");
        assertThat(yubikey, notNullValue());
        assertThat(yubikey.getSerialNumber(), equalTo("123456"));
    }

    /*
    @Test
    public void findByPublicKey() {

    }

    @Test
    void createToken() {
    }

    @Test
    void unlinkYubikey() {
    }

    @Test
    void deleteYubikey() {
    }

    @Test
    void activateYubikey() {
    }

    @Test
    void saveToken() {
    }

    @Test
    void setTokenLockStatus() {
    }

    @Test
    void findByUsername() {
    }

    @Test
    void findAllByUsername() {
    }

    @Test
    void getTokenCount() {
    }
    */
}