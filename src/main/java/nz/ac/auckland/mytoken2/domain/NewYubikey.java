package nz.ac.auckland.mytoken2.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "newyubikeys")
public class NewYubikey {
    @Id
    @Column(name = "public", nullable = false, length = 12)
    private String publicKey;

    @Column(name = "private", nullable = false, length = 12)
    private String privateKey;

    @Column(name = "secret", nullable = false, length = 32)
    private String secretKey;

    @Column(name = "serial", nullable = true, length = 12)
    private String serialNumber;

    public String getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }

    public String getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(String privateKey) {
        this.privateKey = privateKey;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }
}
