package nz.ac.auckland.mytoken2.domain;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "totpkeys")
public class TotpKey {
    @Id
    @Column(name = "id", columnDefinition = "INT(10)")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    private Long id;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created", nullable = false, length = 19)
    @CreationTimestamp
    private Date created;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "accessed", nullable = false, length = 19)
    @UpdateTimestamp
    private Date accessed;

    @Column(name = "active", nullable = false)
    private boolean active;

    @Column(name = "username", nullable = false, length = 100)
    private String username;

    @Column(name = "tokenId", nullable = true, columnDefinition = "TEXT")
    private String tokenId;

    @Column(name = "pin", nullable = true, columnDefinition = "TEXT")
    private String pin;

    @Column(name = "secret", nullable = false, length = 130)
    private String secretKey;

    @Column(name = "digits", nullable = true, length = 10)
    private int digits;

    @Column(name = "bad_logins", nullable = true, length = 10)
    private int badLogins;

    @Column(name = "last_timestep", nullable = true, length = 10)
    private int lastTimestep;

    @Column(name = "algorithm", nullable = false, columnDefinition = "TEXT")
    private String algorithm;

    @Column(name = "timestep", nullable = true, length = 10)
    private int timestep;

    @Column(name = "timestep_origin", nullable = true, length = 10)
    private int timestepOrigin;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getAccessed() {
        return accessed;
    }

    public void setAccessed(Date accessed) {
        this.accessed = accessed;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getTokenId() {
        return tokenId;
    }

    public void setTokenId(String tokenId) {
        this.tokenId = tokenId;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public int getDigits() {
        return digits;
    }

    public void setDigits(int digits) {
        this.digits = digits;
    }

    public int getBadLogins() {
        return badLogins;
    }

    public void setBadLogins(int badLogins) {
        this.badLogins = badLogins;
    }

    public int getLastTimestep() {
        return lastTimestep;
    }

    public void setLastTimestep(int lastTimestep) {
        this.lastTimestep = lastTimestep;
    }

    public String getAlgorithm() {
        return algorithm;
    }

    public void setAlgorithm(String algorithm) {
        this.algorithm = algorithm;
    }

    public int getTimestep() {
        return timestep;
    }

    public void setTimestep(int timestep) {
        this.timestep = timestep;
    }

    public int getTimestepOrigin() {
        return timestepOrigin;
    }

    public void setTimestepOrigin(int timestepOrigin) {
        this.timestepOrigin = timestepOrigin;
    }
}
