package nz.ac.auckland.mytoken2.domain;

public enum TokenType {
    GOOGLE("Software Token"),
    YUBIKEY("Yubikey");

    private String description;

    private TokenType(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public static TokenType get(String type) {
        switch (type.toLowerCase()) {
            case "google":
                return TokenType.GOOGLE;
            case "yubikey":
                return TokenType.YUBIKEY;
        }
        return null;
    }
}
