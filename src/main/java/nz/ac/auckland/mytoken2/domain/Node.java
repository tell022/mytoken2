package nz.ac.auckland.mytoken2.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "radclientlist")
public class Node {
    @Id
    @Column(name = "nasidentifier", nullable = false, length = 64)
    private String ipAddress;

    @Column(name = "secret", nullable = true, length = 32)
    private String secret;

    @Column(name = "defaultrealm", nullable = true, length = 16)
    private String defaultRealm;

    @Column(name = "identifier", nullable = true, length = 32)
    private String identifier;

    @Column(name = "description", nullable = true, length = 128)
    private String description;

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public String getDefaultRealm() {
        return defaultRealm;
    }

    public void setDefaultRealm(String defaultRealm) {
        this.defaultRealm = defaultRealm;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
