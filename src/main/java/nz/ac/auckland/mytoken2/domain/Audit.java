package nz.ac.auckland.mytoken2.domain;

import org.hibernate.annotations.GenericGenerator;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "audit")
public class Audit {
    @Id
    @Column(name = "id", columnDefinition = "INT(10)")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    private Long id;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "timestamp", nullable = false, length = 19)
    private Date timestamp;

    @Column(name = "operator", nullable = false, length = 60)
    private String operator;

    @Column(name = "operator_ip_address", nullable = false, length = 64)
    private String operatorIpAddress;

    @Column(name = "affected_user", nullable = true, length = 60)
    private String affectedUser;

    @Column(name = "token_type", nullable = false, length = 60)
    private String tokenType;

    @Column(name = "serial", nullable = true, length = 12)
    private String serial;

    @Column(name = "tokenId", nullable = true, length = 60)
    private String tokenId;

    @Column(name = "summary", nullable = false, length = 100)
    private String summary;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getOperatorIpAddress() {
        return operatorIpAddress;
    }

    public void setOperatorIpAddress(String operatorIpAddress) {
        this.operatorIpAddress = operatorIpAddress;
    }

    public String getAffectedUser() {
        return affectedUser;
    }

    public void setAffectedUser(String affectedUser) {
        this.affectedUser = affectedUser;
    }

    public String getTokenType() {
        return tokenType;
    }

    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public String getTokenId() {
        return tokenId;
    }

    public void setTokenId(String tokenId) {
        this.tokenId = tokenId;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }
}
