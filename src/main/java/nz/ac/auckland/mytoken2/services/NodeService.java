package nz.ac.auckland.mytoken2.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

import nz.ac.auckland.mytoken2.domain.Node;
import nz.ac.auckland.mytoken2.dto.NodeDetail;
import nz.ac.auckland.mytoken2.repository.NodeRepository;

@Service
public class NodeService {
    private static final Logger log = LoggerFactory.getLogger(NodeService.class);
    private static final int MAX_ROWS = 20;

    private NodeRepository nodeRepository;

    @Autowired
    public NodeService(NodeRepository nodeRepository) {
        this.nodeRepository = nodeRepository;
    }

    public Page<Node> searchNodes(NodeDetail searchQuery) {
        // TODO: allow change to paging
        Example<Node> example = Example.of(searchQuery.asNode());
        return nodeRepository.findAll(example, new PageRequest(1, MAX_ROWS));
    }

    public boolean addNode(Node node) {
        // TODO: Safety checks, find exceptions
        boolean successful = false;
        try {
            nodeRepository.save(node);
            successful = true;
        } catch (Exception e) {
            log.error("Exception occurred attempting to save node {}", e);
        }
        return successful;
    }

    public boolean deleteNodes(List<String> ipAddresses) {
        // TODO: Safety, batching
        boolean successful = false;
        try {
            List<Node> nodes = nodeRepository.findByIpAddressIn(ipAddresses);
            nodeRepository.deleteAll(nodes);
            successful = true;
        } catch (Exception e) {
            log.error("Exception occurred attempting to deletes nodes {}. Exception was: {}", ipAddresses, e);
        }
        return successful;
    }
}
