package nz.ac.auckland.mytoken2.services;

import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.List;
import java.util.Map;

import nz.ac.auckland.mytoken2.domain.TotpKey;
import nz.ac.auckland.mytoken2.domain.Yubikey;
import nz.ac.auckland.mytoken2.dto.TotpKeyDetail;
import nz.ac.auckland.mytoken2.dto.YubikeyDetail;

@Service
public class TokenService {
    private static final String UNKNOWN_NAME_DEFAULT = "";

    private TotpKeyService totpKeyService;
    private YubikeyService yubikeyService;

    public TokenService(TotpKeyService totpKeyService, YubikeyService yubikeyService) {
        this.totpKeyService = totpKeyService;
        this.yubikeyService = yubikeyService;
    }

    /**
     * Find the most recent accessed TokenDetail by username
     *
     * @param username will be used to look up token details
     * @return TotpKeyDetail is the most recent token detail that has been accessed
     */
    @Nullable
    public TotpKeyDetail findMostRecentAccessedTokenDetailByUsername(String username) {
        TotpKey totpKey = totpKeyService.findMostRecentByUsername(username);

        return totpKey == null ? null : TotpKeyDetail.convertFromEntity(totpKey, "");
    }

    /**
     * Find the most recent accessed Yubikey token by username
     *
     * @param username will be used to look up the Yubikey details
     * @return YubikeyDetail is the most recent yubikey token that has been accessed
     */
    public YubikeyDetail findMostRecentAccessedYubikeyDetailByUsername(String username) {
        Yubikey yubikey = yubikeyService.findMostRecentByUsername(username);

        return yubikey == null ? null : YubikeyDetail.convertFromEntity(yubikey, "");
    }

    public int retrieveUserTokenCount(String username) {
        return yubikeyService.getEntityCount(username) + totpKeyService.getTokenCount(username);
    }

    /**
     * Level 2 registration application operations can only occur if the user does
     * not have any tokens.
     *
     * @param username, username of user who is being checked
     * @return true if level 2 registration operation allowed, else false
     */
    public boolean l2RegisterOperationAllowed(String username) {
        return retrieveUserTokenCount(username) == 0;
    }

    public boolean hasToken(String username) {
        return retrieveUserTokenCount(username) > 0;
    }

    public Map<String, List> searchTokens(String searchData) {
        throw new NotImplementedException(); // TODO: method not implemented
    }

    private List<TotpKeyDetail> searchGoogleTokens(List<String> searchColumns, String searchData, int searchLimit) {
        throw new NotImplementedException(); // TODO: method not implemented
    }

    private List<YubikeyDetail> searchAssignedYubikeyTokens(List<String> searchColumns, String searchData, int searchLimit) {
        throw new NotImplementedException(); // TODO: method not implemented
    }

    private List<YubikeyDetail> searchUnassignedYubikeyTokens(List<String> searchColumns, String searchData, int searchLimit) {
        throw new NotImplementedException(); // TODO: method not implemented
    }
}
