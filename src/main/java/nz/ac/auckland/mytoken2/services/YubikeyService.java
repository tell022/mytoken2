package nz.ac.auckland.mytoken2.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.List;

import nz.ac.auckland.mytoken2.domain.NewYubikey;
import nz.ac.auckland.mytoken2.domain.Yubikey;
import nz.ac.auckland.mytoken2.repository.YubikeyRepository;
import nz.ac.auckland.mytoken2.utils.EncodeUtils;

@Service
public class YubikeyService {
    private static final Logger log = LoggerFactory.getLogger(YubikeyService.class);

    private static final int DEFAULT_CLIENT_ID = 0;
    private static final int DEFAULT_COUNTER = 0;
    private static final int DEFAULT_LOW = 0;
    private static final int DEFAULT_HIGH = 0;
    private static final int PUBLIC_KEY_LENGTH = 12;

    private YubikeyRepository yubikeyRepository;
    private NewYubikeyService newYubikeyService;

    @Autowired
    public YubikeyService(YubikeyRepository yubikeyRepository, NewYubikeyService newYubikeyService) {
        this.yubikeyRepository = yubikeyRepository;
        this.newYubikeyService = newYubikeyService;
    }

    @Nullable
    public Yubikey findBySerial(String serialNumber) {
        return yubikeyRepository.findBySerialNumber(serialNumber);
    }

    @Nullable
    public Yubikey findByPublicKey(String publicKey) {
        String tokenId = EncodeUtils.modHexToHex(publicKey.substring(0, PUBLIC_KEY_LENGTH - 1));
        return yubikeyRepository.findByTokenId(tokenId);
    }

    public Yubikey findMostRecentByUsername(String username) {
        List<Yubikey> keys = yubikeyRepository.findFirst1ByUserId(username, new Sort("accessed"));

        return keys.isEmpty() ? null : keys.get(0);
    }

    public Yubikey createNewEntity(String username, String publicKey, String secretKey, String serialNumber, String privateKey) {
        Yubikey yubikey = new Yubikey();
        yubikey.setClientId(DEFAULT_CLIENT_ID);
        yubikey.setActive(true);
        yubikey.setTokenId(EncodeUtils.modHexToHex(publicKey));
        yubikey.setUserId(username);
        yubikey.setSecretKey(secretKey);
        yubikey.setCounter(DEFAULT_COUNTER);
        yubikey.setLow(DEFAULT_LOW);
        yubikey.setHigh(DEFAULT_HIGH);
        yubikey.setSerialNumber(serialNumber);
        yubikey.setPrivateKey(privateKey);
        return yubikey;
    }

    public boolean unlinkYubikey(String serialNumber) {
        Yubikey yubikey = deleteYubikey(serialNumber);

        if (yubikey != null) {
            // Convert from Yubikey to NewYubikey
            NewYubikey newYubikey = new NewYubikey();
            newYubikey.setPublicKey(yubikey.getTokenId());
            newYubikey.setPrivateKey(yubikey.getPrivateKey());
            newYubikey.setSecretKey(yubikey.getSecretKey());
            newYubikey.setSerialNumber(yubikey.getSerialNumber());

            return newYubikeyService.insertNewYubikey(newYubikey);
        } else {
            return false;
        }
    }

    @Nullable
    public Yubikey deleteYubikey(String serialNumber) {
        Yubikey yubikey = findBySerial(serialNumber);

        if (yubikey != null) {
            yubikeyRepository.delete(yubikey);
            log.info("Attempting to delete Yubikey with serial# {}", yubikey.getSerialNumber());
        } else {
            log.info("Failed to delete Yubikey with serial# {}: could not find Yubikey with that serial number", serialNumber);
        }

        return yubikey;
    }

    @Nullable
    public Yubikey activateYubikey(String username, String publicKey) {
        if ( !StringUtils.isEmpty(username) && !StringUtils.isEmpty(publicKey) ) {
            // Retrieve pre-provisioned key based on given public key
            List<NewYubikey> preprovisionedYubikeys = newYubikeyService.retrieveNewYubikey(publicKey.substring(0, PUBLIC_KEY_LENGTH - 1));

            if(preprovisionedYubikeys.size() > 0) {
                NewYubikey provisionedYubikey = preprovisionedYubikeys.get(0);

                // Persist pre-provisioned Yubikey against UPI
                Yubikey createdYubikey = createNewEntity(username, provisionedYubikey.getPublicKey(),
                        provisionedYubikey.getSecretKey(), provisionedYubikey.getSerialNumber(),
                        provisionedYubikey.getPrivateKey());

                yubikeyRepository.save(createdYubikey);
                newYubikeyService.deleteNewYubikey(provisionedYubikey.getSerialNumber());

                return createdYubikey;
            }else {
                // Get serial number of Yubikey from publicKey
                Yubikey existingYubikey = findByPublicKey(publicKey);

                // If any of these fail, cascades down to returning null
                if (existingYubikey != null) {
                    String serial = existingYubikey.getSerialNumber();

                    if (unlinkYubikey(serial)) {
                        // Call back into this method to activate the newly deprovisioned Yubikey
                        return activateYubikey(username, publicKey);
                    }
                }
            }
        }

        return null;
    }

    public Yubikey findActiveByUsername(String username) {
        return yubikeyRepository.findByUserIdAndActive(username, true);
    }

    // From UPIEntityService base, may not be needed
    public Yubikey persistEntity(String upi, String secretKey) {
        throw new NotImplementedException(); // TODO: method not implemented
    }

    public Yubikey setEntityLockStatus(String username, boolean toBeLocked) {
        //TODO: Implement
        throw new NotImplementedException(); // TODO: method not implemented
    }

    public List<Yubikey> findByUsername(String username) {
        throw new NotImplementedException(); // TODO: method not implemented
    }

    public List<Yubikey> findAllByUsername(String username) {
        throw new NotImplementedException(); // TODO: method not implemented
    }

    public int getEntityCount(String username) {
        return yubikeyRepository.countByUserId(username);
    }
}
