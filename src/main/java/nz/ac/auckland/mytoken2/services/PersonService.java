package nz.ac.auckland.mytoken2.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import nz.ac.auckland.mytoken2.dto.Person;

@Service
public class PersonService {
    private static final Logger log = LoggerFactory.getLogger(PersonService.class);

    public String getPersonMobile(String username) {
        Person person = getPerson(username);
        return person.getMobile();
    }

    public String getPersonId (String username) {
        Person person = getPerson(username);
        return person.getId();
    }

    public String getDisplayName(String userId) {
        Person person = getPerson(userId);
        return person.getDisplayName();
    }

    private Person getPerson(String username) {
        return new Person(); // TODO implement;
    }
}
