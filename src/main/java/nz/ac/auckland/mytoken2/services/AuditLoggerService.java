package nz.ac.auckland.mytoken2.services;

import org.slf4j.Logger;
import org.springframework.stereotype.Service;

import nz.ac.auckland.mytoken2.domain.Audit;
import nz.ac.auckland.mytoken2.domain.TokenType;

@Service
public class AuditLoggerService {
    public void log(Logger log, TokenType type, Audit auditData) {
        if (auditData == null) {
            log.error("Tried to log with null auditData");
            return;
        }

        StringBuilder sb = new StringBuilder();
        sb.append(auditData.getTimestamp().toString());
        sb.append(" - ");
        sb.append(auditData.getOperator());
        sb.append(" with IP ");
        sb.append(auditData.getOperatorIpAddress());
        sb.append(" has ");
        sb.append(auditData.getSummary().toLowerCase());
        sb.append(" a ");
        sb.append(auditData.getTokenType());
        sb.append(" token for ");
        sb.append(auditData.getAffectedUser());

        if (type == TokenType.YUBIKEY) {
            sb.append(" (public ID: ");
            sb.append(auditData.getTokenId());
            sb.append(", serial#");
            sb.append(auditData.getSerial());
            sb.append(")");
        }

        log.info(sb.toString());

        // TODO: persist(auditData);
    }
}
