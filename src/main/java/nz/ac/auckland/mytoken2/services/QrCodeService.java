package nz.ac.auckland.mytoken2.services;

import net.glxn.qrgen.javase.QRCode;

import org.apache.commons.codec.binary.Base32;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import nz.ac.auckland.mytoken2.utils.EncodeUtils;

@Service
public class QrCodeService {
    private static final int QR_CODE_SIZE_DEFAULT = 160;

    @Value("${qr.code.data.param.protocol:otpauth://totp/}")
    private String qrCodeDataParamProtocol;
    @Value("${qr.code.identity.suffix:}")
    private String qrCodeIdentitySuffix;
    @Value("${qr.code.data.param.secret:?secret=}")
    private String qrCodeDataParamSecret;
    @Value("${qr.code.data.param.issuer:&issuer=The+University+of+Auckland}")
    private String qrCodeDataParamIssuer;

    private Base32 base32;

    public QrCodeService() {
        base32 = new Base32();
    }

    public String generateQRCodeBase64Data(String username, String secretKey) {
        return generateQRCodeBase64Data(username, secretKey, QR_CODE_SIZE_DEFAULT);
    }

    public String generateQRCodeBase64Data(String username, String secretKey, int qrCodeSize ) {

        byte[] bytes = QRCode.from(constructQRCodeURL( username, secretKey ))
                .withSize( qrCodeSize, qrCodeSize ).stream().toByteArray();

        return EncodeUtils.EncodeBase64(bytes);
    }

    /**
     * Constructs the QR code URL used to generate to generate the QR code.
     * The following format is used:
     * otpauth://totp/{username}{suffix}?secret={secretKey}&issuer={issuer}
     *
     * @param username username of receiving user
     * @param secretKey secret key of Google token
     * @return QR code construction URL
     */
    private String constructQRCodeURL(String username, String secretKey) {
        return qrCodeDataParamProtocol +
               username +
               qrCodeIdentitySuffix +
               qrCodeDataParamSecret +
               base32EncodeHexSecretKey(secretKey) +
               qrCodeDataParamIssuer;
    }

    private String base32EncodeHexSecretKey(String hexSecretKey) {
        return base32.encodeAsString(EncodeUtils.DecodeHexToByteArray(hexSecretKey));
    }
}
