package nz.ac.auckland.mytoken2.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Sort;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.nio.ByteBuffer;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import nz.ac.auckland.mytoken2.domain.TotpKey;
import nz.ac.auckland.mytoken2.repository.TotpKeyRepository;
import nz.ac.auckland.mytoken2.utils.EncodeUtils;

@Service
public class TotpKeyService {
    private static final Logger log = LoggerFactory.getLogger(TotpKeyService.class);

    private static final int BYTES_PER_SECRET_KEY = 20;
    private static final int DEFAULT_BAD_LOGINS = 0;
    private static final int CODE_DIGITS = 6;
    private static final String DEFAULT_ALGORITHM = "SHA1";
    private static final int DEFAULT_TIMESTEP_ORIGIN = 0;
    private static final int TIMESTEP = 30;
    private static final int TOTP_TIME_VARIANCE = 5; // Variation in TIMESTEP second intervals (e.g. 5 equals two and half minutes
    // either side of 30 second timestep interval)

    private TotpKeyRepository totpKeyRepository;

    public TotpKeyService(TotpKeyRepository totpKeyRepository) {
        this.totpKeyRepository = totpKeyRepository;
    }

    @Nullable
    public TotpKey findActiveByUsername(String username) {
        return totpKeyRepository.findByUsernameAndActiveIsTrue(username);
    }

    @Nullable
    public TotpKey findMostRecentByUsername(String username) {
        List<TotpKey> keys = totpKeyRepository.findFirst1ByUsername(username, new Sort("accessed"));

        return keys.isEmpty() ? null : keys.get(0);
    }

    public TotpKey createToken(String upi) {
        // Generate secure random secret key
        SecureRandom randGen = new SecureRandom();
        byte[] secretKey = new byte[BYTES_PER_SECRET_KEY];
        randGen.nextBytes( secretKey );

        return createToken(EncodeUtils.EncodeBytesToHex(secretKey), upi);
    }

    public TotpKey createToken(String username, String secretKey) {
        throw new NotImplementedException(); // TODO: method not implemented
    }

    public TotpKey saveToken(String username, String secretKey) {
        throw new NotImplementedException(); // TODO: method not implemented
    }

    public boolean deleteToken(String username) {
        throw new NotImplementedException(); // TODO: method not implemented
    }

    public TotpKey setTokenLockStatus(String username, boolean toBeLocked) {
        throw new NotImplementedException(); // TODO: method not implemented
    }

    public int getTokenCount(String username) {
        return totpKeyRepository.countByUsername(username);
    }

    /**
     * Verify a given Google Authenticator code (entered by user) against the
     * user's secret key.
     * @param secretKey , user's secret key as part of verification process
     * @param userEnteredCode , code user entered to be verified
     * @return true if successfully verified else false
     */
    public boolean verifyGACode( String secretKey, String userEnteredCode ) {
        boolean isVerifiedSuccessfully = false;

        if ( !StringUtils.isEmpty(userEnteredCode) ) {
            String hmacAlgorithm = "Hmac" + DEFAULT_ALGORITHM;

            // Create key spec with bytes of key (decoded from Hex)
            SecretKeySpec keySpec = new SecretKeySpec(EncodeUtils.DecodeHexToByteArray(secretKey), hmacAlgorithm );
            List<Long> generatedCodes = new ArrayList<>();

            // Allow for some time difference on client device and server time
            for ( int timeVarianceIndex = -TOTP_TIME_VARIANCE; timeVarianceIndex < TOTP_TIME_VARIANCE; timeVarianceIndex++ ) {

                // Generate hash code based on time and key
                ByteBuffer timeIndexBuffer = ByteBuffer.allocate( 8 );

                // Time index (the number of TIMESTEP seconds intervals since Jan 1 1970/Unix time epoch)
                timeIndexBuffer.putLong( (long) (System.currentTimeMillis() / 1000 / TIMESTEP) + timeVarianceIndex );

                try {

                    Mac mac = Mac.getInstance(hmacAlgorithm);
                    mac.init(keySpec);
                    byte[] hash = mac.doFinal(timeIndexBuffer.array());

                    // Google Authenticator specific processing for retrieving code from hash
                    int offset = hash[19] & 0xf;
                    long truncatedHash = hash[offset] & 0x7f;
                    for (int i = 1; i < 4; i++) {
                        truncatedHash <<= 8;
                        truncatedHash |= hash[offset + i] & 0xff;
                    }
                    long generatedCode = truncatedHash % 1000000;
                    generatedCodes.add(generatedCode);
                } catch (NoSuchAlgorithmException | InvalidKeyException e) {
                    log.error("Error generating algorithm {}", e);
                }
            }

            try {
                isVerifiedSuccessfully = generatedCodes.contains( Long.parseLong( userEnteredCode ) );
            }
            catch ( NumberFormatException nfe ) {
                log.info( "User attempted to input a non-numeric value for the TOTP code. {}", nfe );
            }
        }

        return isVerifiedSuccessfully;
    }
}
