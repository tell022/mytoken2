package nz.ac.auckland.mytoken2.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import nz.ac.auckland.mytoken2.domain.UserWebSession;
import nz.ac.auckland.mytoken2.repository.UserWebSessionRepository;

@Service
public class UserWebSessionService {
    private UserWebSessionRepository userWebSessionRepository;

    @Autowired
    public UserWebSessionService(UserWebSessionRepository userWebSessionRepository) {
        this.userWebSessionRepository = userWebSessionRepository;
    }

    /**
     * Add new session pair.
     *
     * @param username, username of user who owns the session
     * @param name,     name of session data
     * @param value,    value of session data
     */
    public void addNewSessionPair(String username, String name, String value) {
        if (!StringUtils.isEmpty(username) && !StringUtils.isEmpty(name)) {
            UserWebSession newSession = new UserWebSession();
            newSession.setActive(true);
            newSession.setUsername(username);
            newSession.setName(name);
            newSession.setValue(value);

            UserWebSession oldSession = findActiveSession(username, name);

            if (oldSession != null) {
                userWebSessionRepository.delete(oldSession);
            }

            userWebSessionRepository.save(newSession);
        }
    }

    /**
     * Deactivate a session pair
     * @param username, username of user whose session will be de-activated
     * @param name, name of session data to de-activate
     */
    public void deactivateSessionPair(String username, String name) {
        UserWebSession webSession = findActiveSession(username, name);

        if (webSession != null) {
            webSession.setActive(false);
            userWebSessionRepository.save(webSession);
        }
    }

    /**
     * Lookup of active session data based on session data name and user's username
     * @param username , username to search against
     * @param name, name of session data to find
     * @return session data
     */
    @Nullable
    public UserWebSession findActiveSession(String username, String name) {
        return userWebSessionRepository.findByUsernameAndName(username, name);
    }
}
