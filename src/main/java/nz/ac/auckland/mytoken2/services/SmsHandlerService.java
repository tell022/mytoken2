package nz.ac.auckland.mytoken2.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import nz.ac.auckland.mytoken2.domain.TokenType;

@Service
public class SmsHandlerService {
    private static final Logger log = LoggerFactory.getLogger(SmsHandlerService.class);

    private PersonService personService;
    //private SmsWsTransmitter smsWsTransmitter;

    @Autowired
    public SmsHandlerService(PersonService personService) {
        this.personService = personService;
        //this.smsWsTransmitter = new SmsWsTransmitter();
    }

    public void sendSMS(String recipientUsername, TokenType tokenType) {
        String mobileNumber = personService.getPersonMobile(recipientUsername);
        String message = getMessageText(recipientUsername, tokenType);
        sendMessage(mobileNumber, message);
    }

    private void sendMessage(String mobilePhoneNumber, String message) {
        if (!StringUtils.isEmpty(mobilePhoneNumber) && !StringUtils.isEmpty(message)) {
            // TODO: Hopefully farming out SMS sending to an API
            //TransmissionDetails transmissionDetails = smsWsTransmitter.transmitSimpleMessage( mobilePhoneNumber, message );

            // Log success of message sent, failed messages are already logged by transmitSimpleMessage() method.
            //if ( transmissionDetails.status == TransmitStatus.SUCCESS ) {
                log.info( "SMS message: $message sent successfully to phone number: $mobilePhoneNumber" );
            //}
        } else {
            log.error("Phone number or message empty");
        }
    }

    private String getMessageText(String recipientUsername, TokenType tokenType) {
        // TODO: Get template message here
        return tokenType.getDescription();
    }
}