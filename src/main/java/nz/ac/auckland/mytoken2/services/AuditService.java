package nz.ac.auckland.mytoken2.services;

import com.querydsl.core.BooleanBuilder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Date;

import nz.ac.auckland.mytoken2.domain.Audit;
import nz.ac.auckland.mytoken2.domain.QAudit;
import nz.ac.auckland.mytoken2.dto.AuditSearchCriteria;
import nz.ac.auckland.mytoken2.repository.AuditRepository;

@Service
public class AuditService {
    private AuditRepository auditRepository;

    @Autowired
    public AuditService(AuditRepository auditRepository) {
        this.auditRepository = auditRepository;
    }

    public Iterable<Audit> retrieveRecords(AuditSearchCriteria auditSearchCriteria) {
        BooleanBuilder query = buildQueryFromCriteria(auditSearchCriteria);
        return auditRepository.findAll(query);
    }

    private BooleanBuilder buildQueryFromCriteria(AuditSearchCriteria criteria) {
        BooleanBuilder booleanBuilder = new BooleanBuilder();
        QAudit qAudit = QAudit.audit;

        String affectedUser = criteria.getAffectedUser();
        String operator = criteria.getOperator();
        String serial = criteria.getSerial();
        String operatorIpAddress = criteria.getOperatorIpAddress();
        String tokenId = criteria.getTokenId();
        Date fromDate = criteria.getFromDate();
        Date toDate = criteria.getToDate();

        if (!StringUtils.isEmpty(affectedUser)) {
            booleanBuilder.and(qAudit.affectedUser.containsIgnoreCase(affectedUser));
        }

        if (!StringUtils.isEmpty(operator)) {
            booleanBuilder.and(qAudit.operator.containsIgnoreCase(operator));
        }

        if (!StringUtils.isEmpty(serial)) {
            booleanBuilder.and(qAudit.serial.containsIgnoreCase(serial));
        }

        if (!StringUtils.isEmpty(operatorIpAddress)) {
            booleanBuilder.and(qAudit.operatorIpAddress.containsIgnoreCase(operatorIpAddress));
        }

        if (!StringUtils.isEmpty(tokenId)) {
            booleanBuilder.and(qAudit.tokenId.containsIgnoreCase(tokenId));
        }

        if (fromDate != null) {
            booleanBuilder.and(qAudit.timestamp.after(fromDate));
        }

        if (toDate != null) {
            booleanBuilder.and(qAudit.timestamp.before(fromDate));
        }

        return booleanBuilder;
    }
}
