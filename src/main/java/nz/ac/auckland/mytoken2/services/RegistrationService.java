package nz.ac.auckland.mytoken2.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import nz.ac.auckland.mytoken2.domain.TokenType;
import nz.ac.auckland.mytoken2.domain.TotpKey;
import nz.ac.auckland.mytoken2.domain.UserWebSession;
import nz.ac.auckland.mytoken2.domain.Yubikey;
import nz.ac.auckland.mytoken2.dto.TotpKeyDetail;

@Service
public class RegistrationService {
    private static final Logger log = LoggerFactory.getLogger(RegistrationService.class);

    private TotpKeyService totpKeyService;
    private YubikeyService yubikeyService;
    private GroupService groupService;
    private SmsHandlerService smsHandlerService;
    private PersonService personService;
    private QrCodeService qrCodeService;
    private UserWebSessionService userWebSessionService;

    @Value("${secret.key:secretKey}")
    private String secretKey;

    @Autowired
    public RegistrationService(TotpKeyService totpKeyService, YubikeyService yubikeyService,
                               GroupService groupService, SmsHandlerService smsHandlerService,
                               PersonService personService, QrCodeService qrCodeService,
                               UserWebSessionService userWebSessionService) {
        this.totpKeyService = totpKeyService;
        this.yubikeyService = yubikeyService;
        this.groupService = groupService;
        this.smsHandlerService = smsHandlerService;
        this.personService = personService;
        this.qrCodeService = qrCodeService;
        this.userWebSessionService = userWebSessionService;
    }

    public TotpKeyDetail getTotpKey(boolean forceNew, String username) {
        TotpKey totpKey;

        if (forceNew) {
            // Creates a new entity regardless of if one already exists or not
            totpKey = totpKeyService.createToken(username);
            userWebSessionService.addNewSessionPair(username, secretKey, totpKey.getSecretKey());

            log.info("Force generation of new TOTPKey data for username ({}).", username);
        } else {
            // Looks for an existing identity
            totpKey = totpKeyService.findActiveByUsername(username);
        }

        if (totpKey == null) totpKey = new TotpKey();

        return TotpKeyDetail.convertFromEntity(totpKey, "");
    }

    public TotpKeyDetail verifyAndPersist(String googleCode, String username) {
        TotpKeyDetail totpKeyDetail = null;

        // Get secret from session
        UserWebSession userWebSession = userWebSessionService.findActiveSession(username, secretKey);

        if (userWebSession != null && totpKeyService.verifyGACode(userWebSession.getValue(), googleCode)) {
            TotpKey totpKey = totpKeyService.saveToken(username, userWebSession.getValue());

            if (totpKey != null) {
                // TODO: Add to MFA individuals by ID
                addMfaIndividual(username);

                smsHandlerService.sendSMS(username, TokenType.GOOGLE);

                // TODO: Push audit logger up to controller

                totpKeyDetail = TotpKeyDetail.convertFromEntity(totpKey, "");

                userWebSessionService.deactivateSessionPair(username, secretKey);
            }
        }

        return totpKeyDetail;
    }

    public boolean activateYubikey(String username, String publicKey) {
        Yubikey yubikey = yubikeyService.activateYubikey(username, publicKey);
        boolean successful = yubikey != null;

        if (successful) {
            String userId = personService.getPersonId(username);
            addMfaIndividual(userId);
            smsHandlerService.sendSMS(username, TokenType.YUBIKEY); // TODO: SMS types or NotificationService
            // TODO: Audit needs to be pushed up into controller
        }

        return successful;
    }

    public String generateQRCodeBase64Data(String username) {
        UserWebSession userActiveSession = userWebSessionService.findActiveSession(username, secretKey);
        String sessionValue = userActiveSession != null ? userActiveSession.getValue() : "";
        return qrCodeService.generateQRCodeBase64Data(username, sessionValue);
    }

    private void addMfaIndividual(String userId) {
        if (!groupService.isMfaUser(userId)) {
            groupService.addUserToMfa(userId);
        }
    }
}
