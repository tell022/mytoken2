package nz.ac.auckland.mytoken2.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import nz.ac.auckland.mytoken2.domain.NewYubikey;
import nz.ac.auckland.mytoken2.repository.NewYubikeyRepository;

@Service
public class NewYubikeyService {
    private static final Logger log = LoggerFactory.getLogger(NewYubikeyService.class);

    private NewYubikeyRepository newYubikeyRepository;

    @Autowired
    public NewYubikeyService(NewYubikeyRepository newYubikeyRepository) {
        this.newYubikeyRepository = newYubikeyRepository;
    }

    public List<NewYubikey> retrieveNewYubikey(String publicKey) {
        return newYubikeyRepository.findAllByPublicKey(publicKey);
    }

    public boolean insertNewYubikey(NewYubikey newYubikey) {
        newYubikeyRepository.save(newYubikey);
        return true; // TODO: Check if we can catch exceptions
    }

    public NewYubikey deleteNewYubikey(String serialNumber) {
        NewYubikey deletedNewYubikey = newYubikeyRepository.findBySerialNumber(serialNumber);

        if (deletedNewYubikey != null) {
            log.info("Attempting to delete unprovisioned Yubikey with serial# {}", deletedNewYubikey.getSerialNumber());
            newYubikeyRepository.delete(deletedNewYubikey);
        } else {
            log.info("Failed to delete unprovisioned Yubikey with serial# {}: could not find Yubikey with that serial number", serialNumber);
        }

        return deletedNewYubikey;
    }

    /* TODO: Unimplemented:
     *  public List<NewYubikey> partialSearch(List<String> searchColumns, List<String> searchValues, Integer maxRows)
     *  private Query<NewYubikey> partialSearchQueryLogic(List<String> searchColumns, List<String> searchValues)
     *  public int searchCount(List<String> searchColumns, List<String> searchValue)
     */
}
