package nz.ac.auckland.mytoken2.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

import nz.ac.auckland.mytoken2.domain.Audit;
import nz.ac.auckland.mytoken2.domain.TokenType;
import nz.ac.auckland.mytoken2.domain.Yubikey;

@Service
public class LockService {
    private static final Logger log = LoggerFactory.getLogger(LockService.class);

    private TotpKeyService totpKeyService;
    private YubikeyService yubikeyService;
    private AuditLoggerService auditLoggerService;

    @Autowired
    public LockService(TotpKeyService totpKeyService, YubikeyService yubikeyService,
                       AuditLoggerService auditLoggerService) {
        this.totpKeyService = totpKeyService;
        this.yubikeyService = yubikeyService;
        this.auditLoggerService = auditLoggerService;
    }

    public boolean lockGoogleTokenForUser(String affectedUser) {
        return setLockStatus(true, affectedUser, TokenType.GOOGLE);
    }

    public boolean unlockGoogleTokenForUser(String affectedUser) {
        return setLockStatus(false, affectedUser, TokenType.GOOGLE);
    }

    public boolean lockYubikeyTokenForUser(String affectedUser) {
        return setLockStatus(true, affectedUser, TokenType.YUBIKEY);
    }

    public boolean unlockYubikeyTokenForUser(String affectedUser) {
        return setLockStatus(false, affectedUser, TokenType.YUBIKEY);
    }

    private boolean setLockStatus(boolean toBeLocked, String affectedUser, TokenType tokenType) {
        Audit audit = new Audit();
        audit.setTimestamp(new Date());
        // TODO: Session information, maybe refactor to pull back into controller
/*        audit.setOperator(sessionHelper.getOperator());
        audit.setOperatorIpAddress(sessionHelper.getOperatorIpAddress());*/
        audit.setAffectedUser(affectedUser);
        audit.setTokenType(tokenType.toString());
        audit.setSummary(toBeLocked ? "Locked" : "Unlocked");

        boolean lockSuccessfully = false;

        Yubikey affectedYubikey = null;

        if ( tokenType == TokenType.GOOGLE ) {
            lockSuccessfully = totpKeyService.setTokenLockStatus( affectedUser, toBeLocked ) != null;
        } else if ( tokenType == TokenType.YUBIKEY ) {
            affectedYubikey = yubikeyService.setEntityLockStatus( affectedUser, toBeLocked );
            lockSuccessfully = affectedYubikey != null;

            if (affectedYubikey != null) {
                audit.setSerial(affectedYubikey.getSerialNumber());
                audit.setSerial(affectedYubikey.getTokenId());
            }
        }

        auditLoggerService.log( log, tokenType, audit);

        return lockSuccessfully;
    }
}
