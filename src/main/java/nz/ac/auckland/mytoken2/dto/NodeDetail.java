package nz.ac.auckland.mytoken2.dto;

import nz.ac.auckland.mytoken2.domain.Node;

public class NodeDetail {
    private String ipAddress;
    private String secret;
    private String identifier;
    private String description;

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public static NodeDetail convertFromEntity(Node node) {
        NodeDetail nodeDetail = new NodeDetail();

        nodeDetail.ipAddress = node.getIpAddress();
        nodeDetail.secret = node.getSecret();
        nodeDetail.identifier = node.getIdentifier();
        nodeDetail.description = node.getDescription();

        return nodeDetail;
    }

    public Node asNode() {
        Node node = new Node();

        node.setIpAddress(this.ipAddress);
        node.setSecret(this.secret);
        node.setIdentifier(this.identifier);
        node.setDescription(this.description);

        return node;
    }
}
