package nz.ac.auckland.mytoken2.dto;

import nz.ac.auckland.mytoken2.domain.Yubikey;
import nz.ac.auckland.mytoken2.utils.EncodeUtils;

import static java.lang.Math.toIntExact;

public class YubikeyDetail {
    private int id;
    private int clientId;
    private String tokenId;
    private String modHexTokenId;
    private String username;
    private String secretKey;
    private int counter;
    private int low;
    private int high;
    private boolean active;
    private String serialNumber;
    private String displayName;

    public static YubikeyDetail convertFromEntity(Yubikey yubikey, String displayName ) {
        YubikeyDetail yubikeyDetail = new YubikeyDetail();

        yubikeyDetail.id = toIntExact(yubikey.getId());
        yubikeyDetail.active = yubikey.isActive();
        yubikeyDetail.clientId = yubikey.getClientId();
        yubikeyDetail.tokenId = yubikey.getTokenId();
        yubikeyDetail.username= yubikey.getUserId();
        yubikeyDetail.secretKey = yubikey.getSecretKey();
        yubikeyDetail.counter = yubikey.getCounter();
        yubikeyDetail.low = yubikey.getLow();
        yubikeyDetail.high = yubikey.getHigh();
        yubikeyDetail.serialNumber = yubikey.getSerialNumber();
        yubikeyDetail.displayName = displayName;
        yubikeyDetail.modHexTokenId = EncodeUtils.hexToModhex(yubikey.getTokenId());

        return yubikeyDetail;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public String getTokenId() {
        return tokenId;
    }

    public void setTokenId(String tokenId) {
        this.tokenId = tokenId;
    }

    public String getModHexTokenId() {
        return modHexTokenId;
    }

    public void setModHexTokenId(String modHexTokenId) {
        this.modHexTokenId = modHexTokenId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    public int getLow() {
        return low;
    }

    public void setLow(int low) {
        this.low = low;
    }

    public int getHigh() {
        return high;
    }

    public void setHigh(int high) {
        this.high = high;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }
}
