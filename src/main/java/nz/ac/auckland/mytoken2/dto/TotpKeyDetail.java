package nz.ac.auckland.mytoken2.dto;

import org.apache.commons.codec.binary.Base32;

import nz.ac.auckland.mytoken2.domain.TotpKey;

public class TotpKeyDetail {
    private Long id;
    private String username;
    private String tokenId;
    private String pin;
    private String secretKey;
    private String base32SecretKey;
    private int digits;
    private int badLogins;
    private int lastTimestep;
    private String algorithm;
    private int timestep;
    private int timestepOrigin;
    private boolean active;
    private String displayName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getTokenId() {
        return tokenId;
    }

    public void setTokenId(String tokenId) {
        this.tokenId = tokenId;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public String getBase32SecretKey() {
        return base32SecretKey;
    }

    public void setBase32SecretKey(String base32SecretKey) {
        this.base32SecretKey = base32SecretKey;
    }

    public int getDigits() {
        return digits;
    }

    public void setDigits(int digits) {
        this.digits = digits;
    }

    public int getBadLogins() {
        return badLogins;
    }

    public void setBadLogins(int badLogins) {
        this.badLogins = badLogins;
    }

    public int getLastTimestep() {
        return lastTimestep;
    }

    public void setLastTimestep(int lastTimestep) {
        this.lastTimestep = lastTimestep;
    }

    public String getAlgorithm() {
        return algorithm;
    }

    public void setAlgorithm(String algorithm) {
        this.algorithm = algorithm;
    }

    public int getTimestep() {
        return timestep;
    }

    public void setTimestep(int timestep) {
        this.timestep = timestep;
    }

    public int getTimestepOrigin() {
        return timestepOrigin;
    }

    public void setTimestepOrigin(int timestepOrigin) {
        this.timestepOrigin = timestepOrigin;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public static TotpKeyDetail convertFromEntity(TotpKey totpKey, String displayName ) {
        TotpKeyDetail totpKeyDetail = new TotpKeyDetail();

        totpKeyDetail.id = totpKey.getId();
        totpKeyDetail.algorithm = totpKey.getAlgorithm();
        totpKeyDetail.badLogins = totpKey.getBadLogins();
        totpKeyDetail.digits = totpKey.getDigits();
        totpKeyDetail.lastTimestep = totpKey.getLastTimestep();
        totpKeyDetail.pin = totpKey.getPin();
        totpKeyDetail.secretKey = totpKey.getSecretKey();
        totpKeyDetail.base32SecretKey = new Base32().encodeAsString( totpKey.getSecretKey().getBytes() );
        totpKeyDetail.timestep = totpKey.getTimestep();
        totpKeyDetail.timestepOrigin = totpKey.getTimestepOrigin();
        totpKeyDetail.tokenId = totpKey.getTokenId();
        totpKeyDetail.username = totpKey.getUsername();
        totpKeyDetail.active = totpKey.isActive();
        totpKeyDetail.displayName = displayName;

        return totpKeyDetail;
    }
}
