package nz.ac.auckland.mytoken2.dto;

import nz.ac.auckland.mytoken2.domain.Audit;

public class AuditDetail {
    private static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
    private String timestamp;
    private String operator;
    private String operatorIpAddress;
    private String affectedUser;
    private String tokenType;
    private String serialNumber;
    private String publicId;
    private String summary;

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getOperatorIpAddress() {
        return operatorIpAddress;
    }

    public void setOperatorIpAddress(String operatorIpAddress) {
        this.operatorIpAddress = operatorIpAddress;
    }

    public String getAffectedUser() {
        return affectedUser;
    }

    public void setAffectedUser(String affectedUser) {
        this.affectedUser = affectedUser;
    }

    public String getTokenType() {
        return tokenType;
    }

    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getPublicId() {
        return publicId;
    }

    public void setPublicId(String publicId) {
        this.publicId = publicId;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public static AuditDetail convertFromEntity(Audit audit) {
        AuditDetail auditDetail = new AuditDetail();
        auditDetail.timestamp = audit.getTimestamp().toString(); // TODO: Format
        auditDetail.operator = audit.getOperator();
        auditDetail.operatorIpAddress = audit.getOperatorIpAddress();
        auditDetail.affectedUser = audit.getAffectedUser();
        auditDetail.tokenType = audit.getTokenType();
        auditDetail.serialNumber = audit.getSerial();
        auditDetail.publicId = audit.getTokenId();
        auditDetail.summary = audit.getSummary();
        return auditDetail;
    }
}
