package nz.ac.auckland.mytoken2.dto;

import java.util.Date;

public class AuditSearchCriteria {
    private String affectedUser;
    private String operator;
    private String serial;
    private String operatorIpAddress;
    private String tokenId;
    private Date fromDate;
    private Date toDate;

    public String getAffectedUser() {
        return affectedUser;
    }

    public void setAffectedUser(String affectedUser) {
        this.affectedUser = affectedUser;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public String getOperatorIpAddress() {
        return operatorIpAddress;
    }

    public void setOperatorIpAddress(String operatorIpAddress) {
        this.operatorIpAddress = operatorIpAddress;
    }

    public String getTokenId() {
        return tokenId;
    }

    public void setTokenId(String tokenId) {
        this.tokenId = tokenId;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }
}
