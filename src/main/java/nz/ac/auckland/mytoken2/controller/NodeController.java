package nz.ac.auckland.mytoken2.controller;

import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

import nz.ac.auckland.mytoken2.domain.Node;
import nz.ac.auckland.mytoken2.dto.NodeDetail;
import nz.ac.auckland.mytoken2.services.NodeService;

@RestController
@RequestMapping("/node")
public class NodeController {
    private NodeService nodeService;

    public NodeController(NodeService nodeService) {
        this.nodeService = nodeService;
    }

    @RequestMapping("/add")
    public boolean add(@RequestBody NodeDetail nodeDetail) {
        Node node = new Node();
        node.setIpAddress(nodeDetail.getIpAddress());
        node.setSecret(nodeDetail.getSecret());
        node.setIdentifier(nodeDetail.getIdentifier());
        node.setDescription(nodeDetail.getDescription());

        return nodeService.addNode(node);
    }

    @RequestMapping("/delete")
    public boolean delete(@RequestBody List<String> ipAddresses) {
        return nodeService.deleteNodes(ipAddresses);
    }

    @RequestMapping("/search")
    public List<NodeDetail> search(@RequestBody NodeDetail nodeDetail) {
        Page<Node> nodes = nodeService.searchNodes(nodeDetail);

        long count = nodes.getTotalElements();
        return nodes.stream()
                .map(NodeDetail::convertFromEntity)
                .collect(Collectors.toList());
    }
}
