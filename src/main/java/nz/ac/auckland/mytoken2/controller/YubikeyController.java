package nz.ac.auckland.mytoken2.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.Date;

import javax.servlet.http.HttpSession;

import nz.ac.auckland.mytoken2.domain.Audit;
import nz.ac.auckland.mytoken2.domain.NewYubikey;
import nz.ac.auckland.mytoken2.domain.TokenType;
import nz.ac.auckland.mytoken2.domain.Yubikey;
import nz.ac.auckland.mytoken2.dto.YubikeyDetail;
import nz.ac.auckland.mytoken2.services.AuditLoggerService;
import nz.ac.auckland.mytoken2.services.NewYubikeyService;
import nz.ac.auckland.mytoken2.services.RegistrationService;
import nz.ac.auckland.mytoken2.services.TokenService;
import nz.ac.auckland.mytoken2.services.YubikeyService;
import nz.ac.auckland.mytoken2.utils.EncodeUtils;
import nz.ac.auckland.mytoken2.utils.SessionUtils;

@RestController
@RequestMapping("/yubikey")
public class YubikeyController {
    private static final Logger log = LoggerFactory.getLogger(YubikeyController.class);

    private YubikeyService yubikeyService;
    private NewYubikeyService newYubikeyService;
    private TokenService tokenService;
    private RegistrationService registrationService;
    private AuditLoggerService auditLoggerService;

    @Autowired
    public YubikeyController(YubikeyService yubikeyService, NewYubikeyService newYubikeyService,
                             RegistrationService registrationService, TokenService tokenService,
                             AuditLoggerService auditLoggerService) {
        this.yubikeyService = yubikeyService;
        this.newYubikeyService = newYubikeyService;
        this.tokenService = tokenService;
        this.registrationService = registrationService;
        this.auditLoggerService = auditLoggerService;
    }

    @RequestMapping("/get")
    public YubikeyDetail get(@RequestParam("username") String username){
        return YubikeyDetail.convertFromEntity(yubikeyService.findActiveByUsername(username), "");
    }

    // Can't seem to find the handler for this method?
    @RequestMapping("/l2Get")
    public void l2Get(){
        throw new NotImplementedException(); // TODO: method not implemented
    }


    // L3 Activate
    @RequestMapping("/activate")
    public boolean activate(@RequestParam String publicKey, HttpSession session) {
        String username = SessionUtils.getOperator(session);
        boolean successful = false;

        if (!StringUtils.isEmpty(username)) {
            successful = registrationService.activateYubikey(username, publicKey);
        } else {
            log.info("No UPI present in Yubikey L3 activate request");
        }

        return successful;
    }

    @RequestMapping("/l2Activate")
    public boolean l2Activate(@RequestParam String publicKey, HttpSession session) {
        String username = SessionUtils.getOperator(session);
        boolean successful = false;

        if (!StringUtils.isEmpty(username) && tokenService.l2RegisterOperationAllowed(username)) {
            successful = registrationService.activateYubikey(username, publicKey);
        } else {
            log.info("Level 2 activate registration option not allowed");
        }

        return successful;
    }

    @RequestMapping("/unlink")
    public boolean unlink(@RequestParam("serialNumber") String serialNumber, HttpSession session) {
        Yubikey yubikey = yubikeyService.findBySerial(serialNumber);

        boolean unlinked = yubikeyService.unlinkYubikey(serialNumber);

        if (unlinked) {
            Assert.notNull(yubikey, "Unlinked Yubikey was null");
            auditYubikey(yubikey.getUserId(), yubikey.getSerialNumber(), yubikey.getTokenId(),
                    SessionUtils.getOperator(session), SessionUtils.getOperatorIp(session), "Unlinked");

            // TODO: Send email
        }

        return unlinked;
    }

    @RequestMapping("/delete")
    public boolean delete(@RequestParam("serialNumber") String serialNumber, HttpSession session) {
        Yubikey yubikey = yubikeyService.deleteYubikey(serialNumber);
        boolean isDeleted = yubikey != null;

        if (isDeleted) {
            String username = yubikey.getUserId();
            if (!StringUtils.isEmpty(username)) {
                // TODO: Send email
            }
            auditYubikey(username, yubikey.getSerialNumber(), yubikey.getTokenId(),
                    SessionUtils.getOperator(session), SessionUtils.getOperatorIp(session), "Deleted");
        } else {
            NewYubikey newYubikey = newYubikeyService.deleteNewYubikey(serialNumber);
            isDeleted = newYubikey != null;

            if (isDeleted) {
                auditYubikey(null, newYubikey.getSerialNumber(), EncodeUtils.modHexToHex(newYubikey.getPublicKey()),
                        SessionUtils.getOperator(session), SessionUtils.getOperatorIp(session), "Deleted");
            }
        }

        if (!isDeleted) log.info("Failed to delete Yubikey with serial number {}", serialNumber);

        return isDeleted;
    }

    private void auditYubikey(String userId, String serial, String tokenId, String operator, String ip, String summary) {
        Audit audit = new Audit();
        audit.setTimestamp(new Date());
        audit.setOperator(operator);
        audit.setOperatorIpAddress(ip);
        audit.setAffectedUser(userId);
        audit.setSerial(serial);
        audit.setTokenId(tokenId);
        audit.setTokenType(TokenType.YUBIKEY.getDescription());
        audit.setSummary(summary);

        auditLoggerService.log(log, TokenType.YUBIKEY, audit);
    }
}
