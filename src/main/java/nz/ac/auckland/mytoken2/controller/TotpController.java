package nz.ac.auckland.mytoken2.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

import javax.servlet.http.HttpSession;

import nz.ac.auckland.mytoken2.domain.Audit;
import nz.ac.auckland.mytoken2.domain.TokenType;
import nz.ac.auckland.mytoken2.domain.TotpKey;
import nz.ac.auckland.mytoken2.dto.TotpKeyDetail;
import nz.ac.auckland.mytoken2.services.AuditLoggerService;
import nz.ac.auckland.mytoken2.services.RegistrationService;
import nz.ac.auckland.mytoken2.services.TokenService;
import nz.ac.auckland.mytoken2.services.TotpKeyService;
import nz.ac.auckland.mytoken2.utils.SessionUtils;

@RestController
@RequestMapping("/totpkey")
public class TotpController {
    private static final Logger log = LoggerFactory.getLogger(TotpController.class);

    private TotpKeyService totpKeyService;
    private TokenService tokenService;
    private RegistrationService registrationService;
    private AuditLoggerService auditLoggerService;

    public TotpController(TotpKeyService totpKeyService, TokenService tokenService,
                          RegistrationService registrationService, AuditLoggerService auditLoggerService) {
        this.totpKeyService = totpKeyService;
        this.tokenService = tokenService;
        this.registrationService = registrationService;
        this.auditLoggerService = auditLoggerService;
    }

    @RequestMapping("/get")
    public TotpKeyDetail get(@RequestParam boolean forceNew, HttpSession session) {
        String username = SessionUtils.getOperator(session);
        TotpKeyDetail responseDetail = new TotpKeyDetail();

        if (!StringUtils.isEmpty(username)) {
            TotpKey totpKey = registrationService.getTotpKey(forceNew, username);
            if (totpKey != null) {
                responseDetail = TotpKeyDetail.convertFromEntity(totpKey, "");
            }
        } else {
            // TODO: send back error
        }

        // TODO: Is wrapped in object with Detail and error string
        return responseDetail;
    }

    @RequestMapping("/l2Get")
    public TotpKeyDetail l2Get(@RequestParam boolean forceNew, HttpSession session) {
        String username = SessionUtils.getOperator(session);
        TotpKeyDetail responseDetail = new TotpKeyDetail();

        if (!StringUtils.isEmpty(username) && tokenService.l2RegisterOperationAllowed(username)) {
            TotpKey totpKey = registrationService.getTotpKey(forceNew, username);
            if (totpKey != null) {
                responseDetail = TotpKeyDetail.convertFromEntity(totpKey, "");
            }
        } else {
            // TODO: send back error
        }

        // TODO: Is wrapped in object with Detail and error string
        return responseDetail;
    }

    @RequestMapping("/verifyAndPersist")
    public TotpKeyDetail verifyAndPersist(@RequestParam String code, HttpSession session) {
        String username = SessionUtils.getOperator(session);
        TotpKeyDetail responseDetail = new TotpKeyDetail();

        if (!StringUtils.isEmpty(username)) {
            TotpKey totpKey = registrationService.verifyAndPersist(code, username);

            if (totpKey != null) {
                responseDetail = TotpKeyDetail.convertFromEntity(totpKey, "");
            } else {
                // TODO: send back error
            }
        } else {
            // TODO: send back error
        }

        // TODO: Is wrapped in object with Detail and error string
        return responseDetail;
    }

    @RequestMapping("/l2VerifyAndPersist")
    public TotpKeyDetail l2VerifyAndPersist(@RequestParam String code, HttpSession session){
        String username = SessionUtils.getOperator(session);

        TotpKeyDetail responseDetail = new TotpKeyDetail();

        if (!StringUtils.isEmpty(username) && tokenService.l2RegisterOperationAllowed(username)) {
            TotpKey totpKey = registrationService.verifyAndPersist(code, username);

            if (totpKey != null) {
                responseDetail = TotpKeyDetail.convertFromEntity(totpKey, "");
            } else {
                // TODO: send back error
            }
        } else {
            // TODO: send back error
        }

        // TODO: Is wrapped in object with Detail and error string
        return responseDetail;
    }

    @RequestMapping("/delete")
    public boolean delete(@RequestParam String username, HttpSession session) {
        boolean result = totpKeyService.deleteToken(username);

        auditTotpKey(username, SessionUtils.getOperator(session), SessionUtils.getOperatorIp(session),
                "Deleted");

        return result;
    }

    private void auditTotpKey(String username, String operator, String ip, String summary) {
        Audit audit = new Audit();
        audit.setTimestamp(new Date());
        audit.setOperator(operator);
        audit.setOperatorIpAddress(ip);
        audit.setAffectedUser(username);
        audit.setTokenType(TokenType.GOOGLE.getDescription());
        audit.setSummary(summary);

        auditLoggerService.log(log, TokenType.GOOGLE, audit);
    }
}
