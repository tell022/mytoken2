package nz.ac.auckland.mytoken2.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import nz.ac.auckland.mytoken2.domain.TokenType;
import nz.ac.auckland.mytoken2.services.LockService;
import nz.ac.auckland.mytoken2.services.TokenService;
import nz.ac.auckland.mytoken2.utils.SessionUtils;

@RestController
@RequestMapping("/token")
public class TokenController {
    private TokenService tokenService;
    private LockService lockService;

    @Autowired
    public TokenController(TokenService tokenService, LockService lockService) {
        this.tokenService = tokenService;
        this.lockService = lockService;
    }

    @RequestMapping("/adminLock")
    public boolean adminLock(@RequestParam String tokenType, @RequestParam String affectedUser) {
        TokenType requestedToken = TokenType.get(tokenType);

        boolean result = false;

        if (requestedToken == TokenType.GOOGLE) {
            result = lockService.lockGoogleTokenForUser(affectedUser);
        } else if (requestedToken == TokenType.YUBIKEY) {
            result = lockService.lockYubikeyTokenForUser(affectedUser);
        }

        if (result) {
            // TODO: email
        }

        return result;
    }

    @RequestMapping("/lock")
    public void lock(@RequestParam String tokenType, HttpSession session) {
        TokenType requestedToken = TokenType.get(tokenType);
        String username = SessionUtils.getOperator(session);

        boolean result = false;

        if (requestedToken == TokenType.GOOGLE) {
            result = lockService.lockGoogleTokenForUser(username);
        } else if (requestedToken == TokenType.YUBIKEY) {
            result = lockService.lockYubikeyTokenForUser(username);
        }

        if (result) {
            // TODO: email
        }
    }

    @RequestMapping("/get")
    public void get() {
        throw new NotImplementedException(); // TODO: method not implemented
    }

    @RequestMapping("/search")
    public void search(@RequestParam String searchData) {
        Map<String, List> tokenSearchResults = tokenService.searchTokens(searchData);

        throw new NotImplementedException(); // TODO: method not implemented
    }

    @RequestMapping("/unlock")
    public boolean unlock(@RequestParam String tokenType, @RequestParam String affectedUser) {
        TokenType requestedToken = TokenType.get(tokenType);
        boolean successful = false;

        if (requestedToken == TokenType.GOOGLE) {
            successful = lockService.unlockGoogleTokenForUser(affectedUser);
        }
        if (requestedToken == TokenType.YUBIKEY) {
            successful = lockService.unlockYubikeyTokenForUser(affectedUser);
        }

        if (successful) {
            // TODO: Email
        }

        return successful;
    }
}
