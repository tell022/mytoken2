package nz.ac.auckland.mytoken2.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;

import nz.ac.auckland.mytoken2.services.QrCodeService;
import nz.ac.auckland.mytoken2.services.RegistrationService;
import nz.ac.auckland.mytoken2.utils.SessionUtils;

@RestController
@RequestMapping("/qrCode")
public class QrCodeController {
    private QrCodeService qrCodeService;
    private RegistrationService registrationService;

    @Autowired
    public QrCodeController(QrCodeService qrCodeService, RegistrationService registrationService) {
        this.qrCodeService = qrCodeService;
        this.registrationService = registrationService;
    }

    @RequestMapping("/get")
    public String get(@RequestParam(defaultValue = "") String secretKey, HttpSession session) {
        String response;
        if (!StringUtils.isEmpty(secretKey)) {
            response = qrCodeService.generateQRCodeBase64Data(SessionUtils.getOperator(session), secretKey);
        } else {
            response = registrationService.generateQRCodeBase64Data();
        }

        return response;
    }
}
