package nz.ac.auckland.mytoken2.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

import nz.ac.auckland.mytoken2.domain.Audit;
import nz.ac.auckland.mytoken2.dto.AuditDetail;
import nz.ac.auckland.mytoken2.dto.AuditSearchCriteria;
import nz.ac.auckland.mytoken2.services.AuditService;

@RestController
@RequestMapping("/audit")
public class AuditController {
    private AuditService auditService;

    @Autowired
    public AuditController(AuditService auditService) {
        this.auditService = auditService;
    }

    @RequestMapping("/search")
    public List<AuditDetail> search(@RequestBody AuditSearchCriteria auditSearchCriteria) {

        Iterable<Audit> auditList = auditService.retrieveRecords(auditSearchCriteria);
        ArrayList<AuditDetail> auditDetails = new ArrayList<>();

        auditList.forEach(audit -> auditDetails.add(AuditDetail.convertFromEntity(audit)));

        return auditDetails;
    }
}
