package nz.ac.auckland.mytoken2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Mytoken2Application {

    public static void main(String[] args) {
        SpringApplication.run(Mytoken2Application.class, args);
    }
}
