package nz.ac.auckland.mytoken2.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

import nz.ac.auckland.mytoken2.domain.Node;

public interface NodeRepository extends JpaRepository<Node, String> {
    List<Node> findByIpAddressIn(List<String> ipAddresses);
}
