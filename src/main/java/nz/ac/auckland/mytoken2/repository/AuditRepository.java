package nz.ac.auckland.mytoken2.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import nz.ac.auckland.mytoken2.domain.Audit;

public interface AuditRepository extends JpaRepository<Audit, Long>, QuerydslPredicateExecutor<Audit> {
}
