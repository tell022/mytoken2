package nz.ac.auckland.mytoken2.repository;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

import nz.ac.auckland.mytoken2.domain.NewYubikey;

public interface NewYubikeyRepository extends CrudRepository<NewYubikey, Long> {
    NewYubikey findBySerialNumber(String serialNumber);
    List<NewYubikey> findAllByPublicKey(String publicKey);
}
