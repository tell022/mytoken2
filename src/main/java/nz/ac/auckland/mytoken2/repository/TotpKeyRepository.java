package nz.ac.auckland.mytoken2.repository;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

import nz.ac.auckland.mytoken2.domain.TotpKey;

public interface TotpKeyRepository extends JpaRepository<TotpKey, Long> {
    TotpKey findByUsernameAndActiveIsTrue(String username);
    List<TotpKey> findFirst1ByUsername(String username, Sort sort);
    int countByUsername(String username);
}
