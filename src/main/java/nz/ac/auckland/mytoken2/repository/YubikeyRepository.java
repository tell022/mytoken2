package nz.ac.auckland.mytoken2.repository;

import org.springframework.data.domain.Sort;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

import nz.ac.auckland.mytoken2.domain.Yubikey;

public interface YubikeyRepository extends PagingAndSortingRepository<Yubikey, Long> {
    Yubikey findBySerialNumber(String serialNumber);
    Yubikey findByTokenId(String tokenId);
    Yubikey findByUserIdAndActive(String userId, boolean active);
    List<Yubikey> findFirst1ByUserId(String userId, Sort sort);
    int countByUserId(String userId);
}
