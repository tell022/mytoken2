package nz.ac.auckland.mytoken2.repository;

import org.springframework.data.repository.CrudRepository;

import nz.ac.auckland.mytoken2.domain.UserWebSession;

public interface UserWebSessionRepository extends CrudRepository<UserWebSession, Long> {
    UserWebSession findByUsernameAndName(String username, String name);
}
