package nz.ac.auckland.mytoken2.utils;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

public class EncodeUtils {
    private static final Logger log = LoggerFactory.getLogger(EncodeUtils.class);

    // Mapping for Yubico's Modhex to standard hex (and vice versa)
    private static Map<String, String> modhexToHexMap = new HashMap<>();
    private static Map<String, String> hexToModhexMap = new HashMap<>();
    static {
        modhexToHexMap.put("c", "0");
        modhexToHexMap.put("b", "1");
        modhexToHexMap.put("d", "2");
        modhexToHexMap.put("e", "3");
        modhexToHexMap.put("f", "4");
        modhexToHexMap.put("g", "5");
        modhexToHexMap.put("h", "6");
        modhexToHexMap.put("i", "7");
        modhexToHexMap.put("j", "8");
        modhexToHexMap.put("k", "9");
        modhexToHexMap.put("l", "a");
        modhexToHexMap.put("n", "b");
        modhexToHexMap.put("r", "c");
        modhexToHexMap.put("t", "d");
        modhexToHexMap.put("u", "e");
        modhexToHexMap.put("v", "f");
        // Populate the map in reverse
        for (String key : modhexToHexMap.keySet()) {
            hexToModhexMap.put(modhexToHexMap.get(key), key);
        }
    }

    public static byte[] DecodeHexToByteArray(String toDecode) {
        byte[] decoded;
        try {
            decoded = Hex.decodeHex(toDecode.toCharArray());
        } catch (DecoderException e) {
            log.error("Encountered error when trying to decode hex {}", toDecode);
            decoded = new byte[0];
        }

        return decoded;
    }

    public static String EncodeBytesToHex(byte[] toEncode) {
        return Hex.encodeHexString(toEncode);
    }

    public static String EncodeBase64(byte[] toEncode) {
        return Base64.encodeBase64String(toEncode);
    }

    public static String modHexToHex(String modHex) {
        return hexConversion(modHex, modhexToHexMap);
    }

    public static String hexToModhex(String hex) {
        return hexConversion(hex, hexToModhexMap);
    }

    private static String hexConversion(String textToConvert, Map map) {
        StringBuilder hexStringBuilder = new StringBuilder();
        for (String character : textToConvert.split("")) {
            hexStringBuilder.append(map.get(character.toLowerCase()));
        }
        return hexStringBuilder.toString();
    }
}
