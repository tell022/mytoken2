package nz.ac.auckland.mytoken2.utils;

import org.springframework.beans.factory.annotation.Value;

import javax.servlet.http.HttpSession;

/* TODO: It may be better to have an interceptor that pulls in the header information */
public class SessionUtils {
    private static boolean runOnLocal;
    private static String remoteUser;

    @Value("${remoteUserSettable:false}")
    public void setRunOnLocal(boolean remoteUserSettable) {
        runOnLocal = remoteUserSettable;
    }

    @Value("${remoteUser}")
    public void setRemoteUser(String remoteUserSet) {
        remoteUser = remoteUserSet;
    }

    public static String getOperator(HttpSession session) {
        if (runOnLocal) {
            return remoteUser;
        } else {
            return session.getAttribute("REMOTE_USER").toString();
        }
    }

    public static String getOperatorIp(HttpSession session) {
        return session.getAttribute("X-FORWARDED-IP").toString();
    }
}